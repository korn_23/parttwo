package parttwo.functional;

import parttwo.functions.AxPlusBandCxPlusD;
import parttwo.functions.interfaceFunc;
import parttwo.functions.AxPlusB;
import parttwo.functions.AsinBx;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LeftRightMidSumTest {
    @Test
    public void testBoundsLin(){
        LeftRightMidSum func = new LeftRightMidSum(0,1);
        interfaceFunc lin = new AxPlusB(1,1,-1,2);
        assertEquals(4.5,func.functional(lin),0.1);
    }
    @Test
    public void testBoundsDiv(){
        LeftRightMidSum func = new LeftRightMidSum(0,1);
        interfaceFunc div = new AxPlusBandCxPlusD(1,1,1,1,-1,2);
        assertEquals(3.0,func.functional(div),0.1);
    }
    @Test
    public void testBoundsSin(){
        LeftRightMidSum func = new LeftRightMidSum(0,1);
        interfaceFunc sin = new AsinBx(1,1,-1,2);
        assertEquals(1.3,func.functional(sin),0.1);
    }

}
