package parttwo.functional;

import org.junit.Test;
import parttwo.functions.AxPlusBandCxPlusD;
import parttwo.functions.interfaceFunc;
import parttwo.functions.AxPlusB;
import parttwo.functions.AsinBx;

import static org.junit.Assert.assertEquals;

public class IntegralTest {
    @Test
    public void testFunctionalLin() {
        Integral fun = new Integral(0, 1);
        interfaceFunc lin = new AxPlusB(1, 0, 0, 1);
        assertEquals(0.5, fun.functional(lin), 0.1);
    }

    @Test
    public void testFunctionalDiv() {
        Integral fun = new Integral(0, 1);
        interfaceFunc div = new AxPlusBandCxPlusD(1, 0, 0, 1, 0, 1);
        assertEquals(0.5, fun.functional(div), 0.1);
    }

    @Test
    public void testFunctionalSin() {
        Integral fun = new Integral(0, Math.PI/2.);
        interfaceFunc sin = new AsinBx(2, 1, 0, 4);
        System.out.println(fun.functional(sin));
        //assertEquals(2,fun.functional(sin), 0.1);
        assertEquals(2,fun.functional(sin), 1e-3);
    }
}
