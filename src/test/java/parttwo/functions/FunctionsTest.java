package parttwo.functions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionsTest {
    @Test
    public void testLinearArity() {
        interfaceFunc lin = new AxPlusB(1, 1, 0, 3);
        assertEquals(lin.getValueAtPoint(1), 2, 10E-9);
    }

    @Test(expected = RuntimeException.class)
    public void testDivArity() {
        interfaceFunc div1 = new AxPlusBandCxPlusD(1, 1, 0, 0, 0, 8);
        div1.getValueAtPoint(1);

    }

    @Test
    public void testDivArityV2() {
        interfaceFunc div1 = new AxPlusBandCxPlusD(2, 3, 2, 3, -1, 1);
        assertEquals(div1.getValueAtPoint(0), 1, 10E-9);
    }


    @Test
    public void testSinArity() {
        interfaceFunc sin = new AsinBx(1, 1, 0, 1);
        assertEquals(sin.getValueAtPoint(0), 0, 10E-9);
    }

    @Test
    public void testExpArity() {
        interfaceFunc ex1 = new AexpXplusB(1, 1, 0, 4);
        assertEquals(3.7, ex1.getValueAtPoint(1), 0.1);
        interfaceFunc ex2 = new AexpXplusB(1, 1, 0, 4);
        assertEquals(2, ex2.getValueAtPoint(0), 10E-9);
    }
}
