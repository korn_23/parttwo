package parttwo.functions;

public interface interfaceFunc {
    double getValueAtPoint(double x);
    double getLeft();
    double getRight();
}
