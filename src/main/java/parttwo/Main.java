package parttwo;

import parttwo.functions.AxPlusB;
import parttwo.functional.Integral;

public class Main {
    public static void main(String[] args) {
        System.out.println("testing...");
        AxPlusB function1Arity = new AxPlusB(1,1,11,1);
        Integral func = new Integral(1,1);
        func.functional(function1Arity);
    }
}
