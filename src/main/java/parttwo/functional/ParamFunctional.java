package parttwo.functional;

import parttwo.functions.interfaceFunc;
public interface ParamFunctional {
    double functional(interfaceFunc func);
}
