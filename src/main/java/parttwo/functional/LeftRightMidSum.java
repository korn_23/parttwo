package parttwo.functional;

import parttwo.functions.interfaceFunc;

public class LeftRightMidSum implements ParamFunctional {
    private double leftBound;
    private double rightBound;

    public LeftRightMidSum(double leftBound, double rightBound) {
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    @Override
    public double functional(interfaceFunc func) {
        if (func.getLeft() > leftBound  || rightBound > func.getRight()) {
            throw new IndexOutOfBoundsException();
        }
        return func.getValueAtPoint(leftBound) + func.getValueAtPoint(rightBound) + func.getValueAtPoint((rightBound + leftBound) / 2);
    }
}
