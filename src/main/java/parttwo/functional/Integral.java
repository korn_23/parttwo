package parttwo.functional;

import parttwo.functions.interfaceFunc;

public class Integral implements ParamFunctional {
    private double leftBound;
    private double rightBound;

    public Integral(double leftBound, double rightBound) {
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    public double functional(interfaceFunc func) {
        if (func.getLeft() > leftBound  || rightBound > func.getRight()) {
            throw new IndexOutOfBoundsException();
        }
        //ситает по методу прямоугольников
        double h = (rightBound - leftBound) / 10000;
        double ans = 0;
        for (double i = 0; i <10000; i ++) {
            ans += func.getValueAtPoint(leftBound + h*i)*h;
        }
        return ans;
    }
}
